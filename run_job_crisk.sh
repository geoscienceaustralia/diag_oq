#PBS -m e
#PBS -P w84
#PBS -q normal
#PBS -l walltime=02:00:00
#PBS -l ncpus=192
#PBS -l mem=256GB
#PBS -l wd
#PBS -N crisk
#PBS -l jobfs=500GB
#PBS -l storage=scratch/y57
#PBS -l other=hyperthread

export OQ_DATADIR=$PBS_JOBS
OUTPUT_PATH="./output_crisk"
module load openquake/3.11.2
oq-ini.all.sh
oq engine --run job_crisk.ini >&  $OUTPUT_PATH/$PBS_JOBID.log
oq engine --export-output 1 $OUTPUT_PATH
mv $OUTPUT_PATH/avg_losses-mean_1.csv $OUTPUT_PATH/avg_losses-mean_$PBS_JOBID.csv

# copy ini
oq show oqparam >& $OUTPUT_PATH/$PBS_JOBID.ini

rm -rf .oqdata.$PBS_JOBIB.gadi-pbs
oq-end.sh


